FROM nikolaik/python-nodejs:latest

ENV PYTHONUNBUFFERED=1

WORKDIR /usr/app/

COPY entrypoint.sh .

EXPOSE 8000

ENTRYPOINT [ "sh", "/usr/app/entrypoint.sh" ]